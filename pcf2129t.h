/**
 * @file 	pcf2129t.h
 * @author	Hugo Schaaf (https://hugoschaaf98@github.com)
 * @brief	Tiny driver for PCF2129T Real Time Clock from NXP
 * @version 1.1
 * @date 	2021-09-02
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#ifndef PCF2129T_H_
#define PCF2129T_H_ 1

#include <array>
#include <cstdbool>
#include <cstdint>

#include "pcf2129t_registers.h"
#include "rtc_common.h"

#include "tortilla.h"

namespace rtc
{

    /**
     * @brief	Pcf2129t driver to configure and interact with NXP's PCF2129T high accuracy Real Time Clock.
	 * @note 	This driver is based on @see tortilla-wrapper to allow easy porting between different MCUs.
     */
    class Pcf2129t
    {

    public:
        /**
		 * @brief CLKOUT possible frequencies.
		 */
        enum class ClkoutFreq_t
        {
            freq32768Hz,
            freq16384Hz,
            freq8192Hz,
            freq4096Hz,
            freq2048Hz,
            freq1024Hz,
            freq1Hz,
            freq0Hz //< No clkout output and CLKOUT pin remains High Impedance
        };

        static constexpr std::uint8_t i2cAddress = 0x51; //< The I2C address of Pcf2129t

        /**
		 * @brief Construct a new Pcf2129t object
		 * 
		 * @param i2c 	An I2C tortilla wrapper instance
		 */
        Pcf2129t(tia::I2C &i2c) : i2c_{i2c},
                                  control1_{0x00}, control2_{0x00}, control3_{0x00},
                                  clkoutCtl_{0x00}, watchdgTimCtl_{0x00}, timestpCtl_{0x00}
        {
            // Here are default settings
            // control1_ |= ;
            // control2_ |= ;
            control3_ |= BIT_U8(CONTROL_3_PWRMNG_0);                                        // Battery switch-over function enable in standard mode, battery low detection disabled.
            clkoutCtl_ |= BIT_U8(CLKOUT_CTL_TCR_1);                                         // Perform temperature measurement every minute.
            watchdgTimCtl_ |= BIT_U8(WATCHDG_TIM_CTL_TI_TP) | BIT_U8(WATCHDG_TIM_CTL_TF_1); // disable watchdog function. Select pulsed mode and 1Hz clock source however
            timestpCtl_ |= BIT_U8(TIMESTP_CTL_TSOFF);                                       // disable timestamp function
        }

        /*** Configuration ***
         * 
         * These methods DO NOT write the RTC's internal registers.
         * They must be invoked before calling configure() 
         * method to customize the default RTC configuration
         * if desired only. 
         */

        /**
         * @brief      Select whether the clock operates in 12H or 24H mode.
         *
         * @param[in]  mode  the count mode
         */
        void selectCountMode(CountMode_t mode);

        /**
         * @brief      Sets the CLKOUT frequency.
         *
         * @param[in]  clkfreq 	The Clkout clock frequency
         */
        void selectClkoutFreq(ClkoutFreq_t clkfreq);

        /*** Setters ***/
        /*
         * These methods DO write the RTC's internal registers.
         */

        /**
         * @brief      Configure the RTC. This method is responsible
         * 			   for writing the configuration prepared with 
         * 			   selectXxxxx() methods to the internal RTC's registers.
         */
        int configure();

        /**
         * @brief      Start the RTC.
         *
         * @return     0 on success or the I2C bus error.
         */
        int start();

        /**
         * @brief      Stop the RTC.
         *
         * @return     0 on success or the I2C bus error.
         */
        int stop();

        /*** TODO ***/

        // void setTemperatureMeasurementPeriod(std::uint8_t mode);
        // void setWatchdogTimer();
        // void setPowerManagementMode(std::uint8_t flags);

        /**
         * @brief      Sets the date and the time in the RTC
         *
         * @param      datetime  The datetime data to write to the RTC.
         *
         * @return     0 on success or the I2C bus error.
         */
        int setDateTime(const Datetime &datetime);

        /*** Getters ***/

        /**
         *  @brief 	Read a single value. Each call to one of the following functions
         *  		perfoms a single read to the I2C bus ie. write the register'address
         *  		to read and then read the value.
         */
        std::uint8_t seconds() { return (bcd_to_dec(readRegister_(SECONDS))); }
        std::uint8_t minutes() { return (bcd_to_dec(readRegister_(MINUTES))); }
        std::uint8_t hours() { return (bcd_to_dec(readRegister_(HOURS))); }
        std::uint8_t day() { return (bcd_to_dec(readRegister_(DAYS))); }
        std::uint8_t weekday() { return (bcd_to_dec(readRegister_(WEEKDAYS))); }
        std::uint8_t month() { return (bcd_to_dec(readRegister_(MONTHS))); }
        std::uint8_t year() { return (bcd_to_dec(readRegister_(YEARS))); }

        /**
         * @brief      Read the date ant time from RTC. The 7 different data registers
         *  			are read in a single I2C data transfer from the RTC.
         *  			
         * @param      datetime  The datetime structure to be filled
         *
         * @return     0 on success or the I2C bus error.
         */
        int dateTime(Datetime &datetime);

    private:
        tia::I2C &i2c_;

        std::uint8_t control1_;
        std::uint8_t control2_;
        std::uint8_t control3_;
        // std::uint8_t _seconds;
        // std::uint8_t _minutes;
        // std::uint8_t _hours;
        // std::uint8_t _days;
        // std::uint8_t _weekdays;
        // std::uint8_t _months;
        // std::uint8_t _years;
        // std::uint8_t _second_alarm;
        // std::uint8_t _minute_alarm;
        // std::uint8_t _hour_alarm;
        // std::uint8_t _day_alarm;
        // std::uint8_t _weekday_alarm;
        std::uint8_t clkoutCtl_;
        std::uint8_t watchdgTimCtl_;
        // std::uint8_t _watchdg_tim_val;
        std::uint8_t timestpCtl_;
        // std::uint8_t _sec_timestp;
        // std::uint8_t _min_timestp;
        // std::uint8_t _hour_timestp;
        // std::uint8_t _day_timestp;
        // std::uint8_t _mon_timestp;
        // std::uint8_t _year_timestp;
        // std::uint8_t _aging_offset;
        // std::uint8_t _internal_reg;

        std::uint8_t readRegister_(std::uint8_t registerAddress)
        {
            i2c_.write(i2cAddress, registerAddress); // write register address
            return i2c_.read(i2cAddress);
        }

        template <std::size_t RxSize>
        int readMultipleRegisters_(std::uint8_t registerAddress, std::array<std::uint8_t, RxSize> &rxBuf)
        {
            auto err = i2c_.write(i2cAddress, registerAddress); // write register address
            if (not err)
                err = i2c_.read(i2cAddress, rxBuf);
            return err;
        }

        int writeRegister_(std::uint8_t registerAddress, std::uint8_t value)
        {
            return i2c_.write(i2cAddress, std::array<std::uint8_t, 2>{registerAddress, value}); // write register address
        }

        // This is a bit special because we need to include reister address in txBuf[0]
        template <std::size_t TxSize>
        int writeMultipleRegisters_(const std::array<std::uint8_t, TxSize> &txBuf)
        {
            return i2c_.write(i2cAddress, txBuf); // write register address
        }
    };

} // namespace rtc

#endif // PCF2129T_H_
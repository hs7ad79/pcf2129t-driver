/**
 * @file 	rtc_common.h
 * @author	Hugo Schaaf (https://hugoschaaf98@github.com)
 * @brief	Common things and utils to Real Time Clocks
 * @version 1.0
 * @date 	2021-09-03
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#ifndef RTC_COMMON_H_
#define RTC_COMMON_H_

#include <cstdint>

namespace rtc
{
    /**
	 * @brief       Structure which stores Date and Time data retrived from RTC
	 * 				in decimal format.
	 */
    struct Datetime
    {
        std::uint8_t sec;  //< seconds
        std::uint8_t min;  //< minutes
        std::uint8_t hour; //< hours
        std::uint8_t day;  //< days
        std::uint8_t wday; //< weekdays
        std::uint8_t mon;  //< months
        std::uint8_t year; //< years
    };

    /**
     * @brief      12h or 24h mode selection to the RTC.
     */
    enum class CountMode_t
    {
        mode12h,
        mode24h
    };

    /**
     * @brief      Convert BCD formatted value to decimal formatted value.
     *
     * @param[in]  bcd   The bcd value to convert into decimal value.
     *
     * @return     The decimal corresponding value
     */
    inline std::uint8_t
    bcd_to_dec(std::uint8_t bcd) { return ((bcd & 0x0F) + (bcd >> 4) * 10); }

    /**
     * @brief      Convert decimal formatted value to BCD formatted value.
     *
     * @param[in]  dec   The decimal value to convert into BCD value.
     *
     * @return     The decimal corresponding value
     */
    inline std::uint8_t
    dec_to_bcd(std::uint8_t dec) { return ((dec % 10) | ((dec / 10) << 4)); }

} // namespace rtc

#endif
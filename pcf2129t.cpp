/**
 * @file 	pcf2129t.cpp
 * @author	Hugo Schaaf (https://hugoschaaf98@github.com)
 * @brief	Tiny driver for PCF2129T Real Time Clock from NXP
 * @version 1.1
 * @date 	2021-09-02
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include "pcf2129t.h"

namespace rtc
{
    /**
	 * @brief      Write the configuration to the RTC. 
	 * 
	 * @note 	   This functions returns immediately
	 * 			   on I2C transfer fail without continuing the configuration.
	 *
	 * @return     0 on success or the I2C bus error.
	 */
    int Pcf2129t::configure()
    {
        auto err = writeRegister_(CONTROL_1, CONTROL_1_FORMAT(control1_));
        if (not err)
            err = writeRegister_(CONTROL_2, CONTROL_2_FORMAT(control2_));
        if (not err)
            err = writeRegister_(CONTROL_3, CONTROL_3_FORMAT(control3_));
        if (not err)
            err = writeRegister_(CLKOUT_CTL, CLKOUT_CTL_FORMAT(clkoutCtl_));
        if (not err)
            err = writeRegister_(WATCHDG_TIM_CTL, WATCHDG_TIM_CTL_FORMAT(watchdgTimCtl_));
        if (not err)
            err = writeRegister_(TIMESTP_CTL, TIMESTP_CTL_FORMAT(timestpCtl_));
        return err;
    }

    /**
	 * @brief      Start the RTC.
	 *
	 * @return     0 on success or the I2C bus error.
	 */
    int Pcf2129t::start()
    {
        control1_ &= ~BIT_U8(CONTROL_1_STOP); // clear the stop bit to start the RTC
        return writeRegister_(CONTROL_1, CONTROL_1_FORMAT(control1_));
    }

    /**
	 * @brief      Stop the RTC.
	 *
	 * @return     0 on success or the I2C bus error.
	 */
    int Pcf2129t::stop()
    {
        control1_ |= BIT_U8(CONTROL_1_STOP); // set the stop bit to stop the RTC
        return writeRegister_(CONTROL_1, CONTROL_1_FORMAT(control1_));
    }

    /**
     * @brief      Select whether the clock operates in 12H or 24H mode.
     *
     * @param[in]  mode  the count mode
     */
    void Pcf2129t::selectCountMode(CountMode_t mode)
    {
        if (mode == CountMode_t::mode12h)
        {
            control1_ |= BIT_U8(CONTROL_1_12_24); // set 12_24 bit
        }
        else if (mode == CountMode_t::mode24h)
        {
            control1_ &= ~BIT_U8(CONTROL_1_12_24); // clear 12_24 bit
        }
    }

    /**
	 * @brief      Sets the CLKOUT frequency.
	 *
	 * @param[in]  clkfreq 	The Clkout clock frequency
	 */
    void Pcf2129t::selectClkoutFreq(ClkoutFreq_t clkfreq)
    {
        clkoutCtl_ &= 0xF8; // reset COF[2:0] bits

        switch (clkfreq)
        {
        case ClkoutFreq_t::freq32768Hz:
            break;
        case ClkoutFreq_t::freq16384Hz:
            clkoutCtl_ |= BIT_U8(CLKOUT_CTL_COF_0);
            break;
        case ClkoutFreq_t::freq8192Hz:
            clkoutCtl_ |= BIT_U8(CLKOUT_CTL_COF_1);
            break;
        case ClkoutFreq_t::freq4096Hz:
            clkoutCtl_ |= BIT_U8(CLKOUT_CTL_COF_0) | BIT_U8(CLKOUT_CTL_COF_1);
            break;
        case ClkoutFreq_t::freq2048Hz:
            clkoutCtl_ |= BIT_U8(CLKOUT_CTL_COF_2);
            break;
        case ClkoutFreq_t::freq1024Hz:
            clkoutCtl_ |= BIT_U8(CLKOUT_CTL_COF_2) | BIT_U8(CLKOUT_CTL_COF_0);
            break;
        case ClkoutFreq_t::freq1Hz:
            clkoutCtl_ |= BIT_U8(CLKOUT_CTL_COF_1) | BIT_U8(CLKOUT_CTL_COF_2);
            break;
        default: // no output by default - freq0HZ and CLKOUT pin is High Impedance
            clkoutCtl_ |= BIT_U8(CLKOUT_CTL_COF_0) | BIT_U8(CLKOUT_CTL_COF_1) | BIT_U8(CLKOUT_CTL_COF_2);
            break;
        }
    }

    /**
	 * @brief      Sets the date and the time in the RTC
	 *
	 * @param      datetime  The datetime data to write to the RTC.
	 *
	 * @return     0 on success or the I2C bus error.
	 */
    int Pcf2129t::setDateTime(const Datetime &datetime)
    {
        // convert each Datetime struct member into BCD format
        std::array<std::uint8_t, 8> data{
            SECONDS, // we must include the register address
            dec_to_bcd(datetime.sec),
            dec_to_bcd(datetime.min),
            dec_to_bcd(datetime.hour),
            dec_to_bcd(datetime.day),
            dec_to_bcd(datetime.wday),
            dec_to_bcd(datetime.mon),
            dec_to_bcd(datetime.year),
        };
        return writeMultipleRegisters_(data);
    }

    /**
	 * @brief      Read the date ant time from RTC. The 7 different data registers
	 *  			are read in a single I2C data transfer from the RTC.
	 *  			
	 * @param      datetime  The datetime structure to be filled
	 *
	 * @return     0 if success, -1 if an error occured during the I2C transfer.
	 */
    int Pcf2129t::dateTime(Datetime &datetime)
    {
        std::array<std::uint8_t, 7> data{}; // temporary buffer to store the 7 bytes representing the date & time

        // retrieve date time from RTC
        auto err = readMultipleRegisters_(SECONDS, data);

        if (not err) // if data read successfully
        {
            // convert each received byte of data into decimal format
            for (auto &byte : data)
            {
                byte = bcd_to_dec(byte);
            }

            // store converted data in datetime structure
            datetime.sec  = data[0];
            datetime.min  = data[1];
            datetime.hour = data[2];
            datetime.day  = data[3];
            datetime.wday = data[4];
            datetime.mon  = data[5];
            datetime.year = data[6];
        }
        return err;
    }

} // namespace RTC